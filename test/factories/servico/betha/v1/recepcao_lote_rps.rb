FactoryGirl.define do
	factory :br_nfe_servico_betha_recepcao_lote_rps, class:  BrNfe::Servico::Betha::V1::RecepcaoLoteRps do
		numero_lote_rps '123'
		operacao        '1'		
	end
end